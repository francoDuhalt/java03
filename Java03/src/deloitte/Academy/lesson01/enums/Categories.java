package deloitte.Academy.lesson01.enums;
/**
 * 
 * @author Franco Duhalt
 *
 */
public enum Categories {
	Celulares, Electronicos, Hogar;
}
