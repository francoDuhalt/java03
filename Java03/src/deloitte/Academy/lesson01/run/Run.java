package deloitte.Academy.lesson01.run;

import java.util.ArrayList;
import java.util.logging.Logger;

import deloitte.Academy.lesson01.enums.Categories;
import deloitte.Academy.lesson01.operation.Products;
import deloitte.Academy.lesson01.operation.Tasks;

public class Run {
	private static final Logger LOGGER = Logger.getLogger(Run.class.getName());
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Products> soldProducts = new ArrayList<Products>();
		ArrayList<Products> productList = new ArrayList<Products>();
		
		
		
		Products remaingProduct1 = new Products();
		Products remaingProduct2 = new Products();
		Products newProduct = new Products();
		
		String soldWord = "Mouse";
		String soldCategory = "Electronicos";
		
		String remainingWord = "Sofa";
		String remainingCategory = "Hogar";
		String sellProduct ="IPhone X";
		int sellQuntity = 4;
		
		remaingProduct1.setName("Mouse");
		remaingProduct1.setCategory(Categories.Electronicos.toString());
		remaingProduct1.setQuantity(20);
		
		remaingProduct2.setName("Sofa");
		remaingProduct2.setCategory(Categories.Hogar.toString());
		remaingProduct2.setQuantity(10);
		
		newProduct.setName("IPhone X");
		newProduct.setCategory(Categories.Celulares.toString());
		newProduct.setQuantity(15);
		
		productList.add(remaingProduct1);
		productList.add(remaingProduct2);
		
		
		
		LOGGER.info(Tasks.add(productList, newProduct));
		LOGGER.info(Tasks.search(productList, remainingWord));
		LOGGER.info(Tasks.search(productList, remainingCategory));
		
		LOGGER.info("La cantidad total es: "+ (Tasks.totalProducts(productList)));
		
		LOGGER.info(Tasks.buy(productList, newProduct, soldProducts, sellProduct, sellQuntity));
		
		LOGGER.info("La cantidad total es: "+ (Tasks.totalProducts(soldProducts)));
		LOGGER.info(Tasks.search(soldProducts, remainingWord));
		LOGGER.info(Tasks.search(soldProducts, remainingCategory));
		
		
		
		
		
	
		
		
		
		
		
	}

}
