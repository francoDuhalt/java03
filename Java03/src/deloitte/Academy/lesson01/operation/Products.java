package deloitte.Academy.lesson01.operation;

import deloitte.Academy.lesson01.enums.Categories;

/**
 * 
 * @author Franco Duhalt
 *
 */
public class Products {
	public String name;
	public String category;
	public int quantity;
	
	
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	
}
