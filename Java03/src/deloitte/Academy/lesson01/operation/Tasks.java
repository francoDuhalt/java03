package deloitte.Academy.lesson01.operation;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Franco Duhalt
 *
 */
public class Tasks {
	private static final Logger LOGGER = Logger.getLogger(Tasks.class.getName());
	/**
	 * 
	 * @param products of type Products
	 * @return products with data name and data category
	 */
	public Products registrar(Products products) {
		Products objProduct = new Products();
		try {
			objProduct.setCategory(products.getCategory());
			objProduct.setName(products.getName());
			objProduct.setQuantity(products.getQuantity());
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		
		return products;
		
	}
	
	/**
	 * 
	 * @param productList
	 * @param newProducts
	 * @return add newProducts to productList
	 */
	
	public static String add(ArrayList<Products> productList, Products newProducts) {
		String result = "";
		try {
			productList.add(newProducts);
			result ="El producto se ha agregado";
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;
	}
	
	/**
	 * 
	 * @param productList
	 * @param newProducts
	 * @param soldProductList
	 * @param soldProduct
	 * @param quantity
	 * @return a sell product
	 */
	public static String buy(ArrayList<Products> productList, Products newProducts, ArrayList<Products> soldProductList, String soldProduct, int quantity) {
		String result = "";
		try {
			for (Products products: productList) {
				if (products.getName() == soldProduct) {
					
					int newQuantity = products.getQuantity() - quantity;
					products.setQuantity(newQuantity);
				}
			}
			newProducts.setQuantity(quantity);
			soldProductList.add(newProducts);
			
			result ="Se ha vendido un producto";
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;
	}
	
	/**
	 * 
	 * @param productList
	 * @return productList size
	 */
	public static int totalProducts(ArrayList<Products> product){
		int resultado = 0;
		
		try {
			for (Products products: product) {
				
				resultado += products.getQuantity();
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		
		return resultado;
	}
	
	/**
	 * 
	 * @param product
	 * @param word
	 * @return product search
	 */
	public static String search(ArrayList<Products> product, String word) {

		String result = "";
		product.toString();

		try {
			for (Products products: product) {
				if (products.getName().toString() == word) {
					result = "Se ha encontrado el producto.";
				}else if (products.getCategory() == word) {
					result = "Se ha encontrado la categor�a.";
				}else {
					result = "No se ha encontrado nada.";
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;
	}
	/**
	 * 
	 * @param product
	 * @return productList
	 */
	
	public static String printList(ArrayList<Products> product) {
		String result = "";
		for (Products products: product) {
			
			result = ("Nombre: " + products.getName().toString() + ","+ " " + "Categoria: " + products.getCategory().toString() +  "," + " Cantidad: " + products.getQuantity());
		}
		return result;
	}
	
}
